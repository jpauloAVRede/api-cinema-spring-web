package br.com.zup.APICinema.dtos;

import br.com.zup.APICinema.enums.Categoria;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class FilmeDTO {
    @Size(min = 2, max = 50, message = "{validacao.nomeFilme}")
    private String nome;
    @Size(min = 30, max = 200, message = "{validacao.sinopse}")
    private String sinopse;
    @DateTimeFormat(pattern = "yyyy")
    private LocalDate anoDeLancamento;
    @Valid
    private List<AtorDTO> elenco = new ArrayList<>();
    private Categoria categoria;

    public FilmeDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSinopse() {
        return sinopse;
    }

    public void setSinopse(String sinopse) {
        this.sinopse = sinopse;
    }

    public LocalDate getAnoDeLancamento() {
        return anoDeLancamento;
    }

    public void setAnoDeLancamento(LocalDate anoDeLancamento) {
        this.anoDeLancamento = anoDeLancamento;
    }

    public List<AtorDTO> getElenco() {
        return elenco;
    }

    public void setElenco(List<AtorDTO> elenco) {
        this.elenco = elenco;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }
}
