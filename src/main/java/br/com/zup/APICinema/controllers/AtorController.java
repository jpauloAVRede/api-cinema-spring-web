package br.com.zup.APICinema.controllers;

import br.com.zup.APICinema.dtos.AtorDTO;
import br.com.zup.APICinema.dtos.PesqFilmeNomeAnoDTO;
import br.com.zup.APICinema.services.AtorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/atores")
public class AtorController {

    @Autowired
    private AtorService atorService;

    @PostMapping
    public List<AtorDTO> adicionarAtor(@RequestBody @Valid AtorDTO atorDTO){
        return atorService.adicionarAtorLista(atorDTO);
    }

    @GetMapping("/{nomeAtor}")
    public List<PesqFilmeNomeAnoDTO> pesquisaFilmePorAtor(@PathVariable String nomeAtor){
        return atorService.pesquisaFilmePorAtor(nomeAtor);
    }

}
