package br.com.zup.APICinema.services;

import br.com.zup.APICinema.dtos.AtorDTO;
import br.com.zup.APICinema.dtos.FilmeDTO;
import br.com.zup.APICinema.dtos.PesqFilmeNomeAnoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AtorService {

    @Autowired
    private FilmeService filmeService;
    private List<AtorDTO> listaAtores = new ArrayList<>();

    public List<AtorDTO> adicionarAtorLista(AtorDTO atorDTO){
        listaAtores.add(atorDTO);
        return listaAtores;
    }

    public AtorDTO pesquisaAtor(String nomePesquisa){
        for (AtorDTO elemLista : this.listaAtores){
            if (elemLista.getNome().equals(nomePesquisa)){
                return elemLista;
            }
        }
        throw new RuntimeException("Ator não cadastrado no LinkeDin");
    }

    public List<PesqFilmeNomeAnoDTO> pesquisaFilmePorAtor(String nomeAtor){
        List<PesqFilmeNomeAnoDTO> filmeTemporario = new ArrayList<>();

        for (FilmeDTO elemLista : filmeService.getCatalogoFilmes()){
            for (AtorDTO elemAtor : elemLista.getElenco()){
                if (elemAtor.getNome().equals(nomeAtor)){
                    filmeTemporario.add(new PesqFilmeNomeAnoDTO(elemLista.getNome(), elemLista.getAnoDeLancamento()));
                }
            }
        }
        return filmeTemporario;
    }




}
