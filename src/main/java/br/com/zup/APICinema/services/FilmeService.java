package br.com.zup.APICinema.services;

import br.com.zup.APICinema.dtos.AtorDTO;
import br.com.zup.APICinema.dtos.FilmeDTO;
import br.com.zup.APICinema.dtos.PesqFilmeNomeAnoDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

@Service
public class FilmeService {
    @Autowired
    private AtorService atorService;
    private List<FilmeDTO> catalogoFilmes = new ArrayList<>();

    public List<FilmeDTO> getCatalogoFilmes() {
        return catalogoFilmes;
    }

    public List<FilmeDTO> adicionarFilmes(FilmeDTO filmeDTO){
        List<AtorDTO> listAtorAtualizado = new ArrayList<>();

        for (AtorDTO elementLista : filmeDTO.getElenco()){
            listAtorAtualizado.add(atorService.pesquisaAtor(elementLista.getNome()));
        }
        filmeDTO.setElenco(listAtorAtualizado);
        catalogoFilmes.add(filmeDTO);

        return catalogoFilmes;
    }

    public int verificaPeriodoLancamento(LocalDate dataLancamento){
        Period periodo = Period.between(dataLancamento, LocalDate.now());
        return periodo.getYears();
    }

    public List<PesqFilmeNomeAnoDTO> pesqFilmesRecente(){
        List<PesqFilmeNomeAnoDTO> listaPesqFilme = new ArrayList<>();

        for (FilmeDTO elemLista : this.catalogoFilmes){
            if (verificaPeriodoLancamento(elemLista.getAnoDeLancamento()) < 2) {
                listaPesqFilme.add(new PesqFilmeNomeAnoDTO(elemLista.getNome(), elemLista.getAnoDeLancamento()));
            }
        }
        return listaPesqFilme;
    }

    public FilmeDTO pesquisaFilmeNome(String nome){
        for (FilmeDTO elemLista : this.catalogoFilmes){
            if (elemLista.getNome().equals(nome)){
                return elemLista;
            }
        }
        throw new RuntimeException("Filme não encontrado");
    }

}
