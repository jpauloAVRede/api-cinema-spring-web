package br.com.zup.APICinema.dtos;

import java.time.LocalDate;

public class PesqFilmeNomeAnoDTO {
    private String nome;
    private LocalDate dataLancamento;

    public PesqFilmeNomeAnoDTO() {
    }

    public PesqFilmeNomeAnoDTO(String nome, LocalDate dataLancamento) {
        this.nome = nome;
        this.dataLancamento = dataLancamento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataLancamento() {
        return dataLancamento;
    }

    public void setDataLancamento(LocalDate dataLancamento) {
        this.dataLancamento = dataLancamento;
    }
}
