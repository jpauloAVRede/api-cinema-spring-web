package br.com.zup.APICinema.controllers;

import br.com.zup.APICinema.dtos.FilmeDTO;
import br.com.zup.APICinema.dtos.PesqFilmeNomeAnoDTO;
import br.com.zup.APICinema.services.FilmeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/filmes")
public class FilmeController {

    @Autowired
    private FilmeService filmeService;

    @PostMapping
    public List<FilmeDTO> adicionarFilme(@RequestBody @Valid FilmeDTO filmeDTO){
        return filmeService.adicionarFilmes(filmeDTO);
    }

    @GetMapping
    public List<PesqFilmeNomeAnoDTO> pesqFilmesRecente(){
        return this.filmeService.pesqFilmesRecente();
    }

    @GetMapping("/pesquisa/{nome}")
    public FilmeDTO pesquisaFilmeNome(@PathVariable String nome){
        return this.filmeService.pesquisaFilmeNome(nome);
    }

}
