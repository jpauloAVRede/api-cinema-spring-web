package br.com.zup.APICinema.dtos;

import javax.validation.constraints.Max;
import javax.validation.constraints.Size;

public class AtorDTO {
    @Size(min = 2, max = 50, message = "{validacao.nomeAtor}")
    private String nome;
    private int idade;

    public AtorDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    @Override
    public String toString() {
        return "AtorDTO{" +
                "nome='" + nome + '\'' +
                ", idade=" + idade +
                '}';
    }
}
