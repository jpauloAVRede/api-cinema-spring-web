package br.com.zup.APICinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiCinemaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiCinemaApplication.class, args);
	}

}
